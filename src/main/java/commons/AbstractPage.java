package commons;

import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static java.time.Duration.ofSeconds;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.swing.plaf.MenuBarUI;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.DashboardPageUI;
import myaccountantUI.ReportPageUI;
import myaccountantUI.SwipeBarMenuUI;

public class AbstractPage {

	AndroidElement element, element1;
	WebDriverWait waitExplicit;

	@SuppressWarnings("rawtypes")
	TouchAction touch;

	By byLocator;
	long shortTimeout = 10;
	long longTimeout = 30;

	public void clickToElement(AndroidDriver<AndroidElement> driver, String locator) {
		element = driver.findElement(By.xpath(locator));
		element.click();
	}

	public void clickToElement(AndroidDriver<AndroidElement> driver, String locator, String... values) {
		locator = String.format(locator, (Object[]) values);
		element = driver.findElement(By.xpath(locator));
		element.click();
	}

	public void clearElement(AndroidDriver<AndroidElement> driver, String locator) {
		element = driver.findElement(By.xpath(locator));
		element.clear();
	}

	public void clearElement(AndroidDriver<AndroidElement> driver, String locator, String... values) {
		locator = String.format(locator, (Object[]) values);
		element = driver.findElement(By.xpath(locator));
		element.clear();
	}

	public void waitForElementVisible(AndroidDriver<AndroidElement> driver, String locator) {
		waitExplicit = new WebDriverWait(driver, longTimeout);
		byLocator = By.xpath(locator);
		try {
			sleepInSecond(1);
			waitExplicit.until(ExpectedConditions.visibilityOfElementLocated(byLocator));
		} catch (Exception ex) {
			Reporter.log("============================================================Wait for element not visible");
			Reporter.log(ex.getMessage());
			System.err.println(ex.getMessage() + "\n");
		}

	}

	public void waitForElementInVisible(AndroidDriver<AndroidElement> driver, String locator) {
		Date date = new Date();
		byLocator = By.xpath(locator);
		waitExplicit = new WebDriverWait(driver, shortTimeout);
		overrideGlobalTimeout(driver, shortTimeout);
		System.out.println("Start time for wait invisible =" + date.toString());
		waitExplicit.until(ExpectedConditions.invisibilityOfElementLocated(byLocator));
		System.out.println("End time for wait invisible =" + new Date().toString());
		overrideGlobalTimeout(driver, longTimeout);
	}

	public void waitForElementVisible(AndroidDriver<AndroidElement> driver, String locator, String... values) {
		waitExplicit = new WebDriverWait(driver, longTimeout);
		locator = String.format(locator, (Object[]) values);
		byLocator = By.xpath(locator);
		try {
			sleepInSecond(1);
			waitExplicit.until(ExpectedConditions.visibilityOfElementLocated(byLocator));
		} catch (Exception ex) {
			Reporter.log("============================================================Wait for element not visible");
			Reporter.log(ex.getMessage());
			System.err.println(ex.getMessage() + "\n");
		}
	}

	public void overrideGlobalTimeout(AndroidDriver<AndroidElement> driver, long timeOut) {
		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
	}

	public void sleepInSecond(long timeInSecond) {
		try {
			Thread.sleep(timeInSecond * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void sendkeyToElement(AndroidDriver<AndroidElement> driver, String locator, String valueToSendkey) {
		element = driver.findElement(By.xpath(locator));
		element.sendKeys(valueToSendkey);
	}

	public void sendkeyToElement(AndroidDriver<AndroidElement> driver, String locator, String valueToSendkey,
			String... values) {
		locator = String.format(locator, (Object[]) values);
		element = driver.findElement(By.xpath(locator));
		element.sendKeys(valueToSendkey);
	}

	public String getAttributeValue(AndroidDriver<AndroidElement> driver, String locator, String attributeName) {
		element = driver.findElement(By.xpath(locator));
		return element.getAttribute(attributeName);
	}
	
	public String getAttributeValue(AndroidDriver<AndroidElement> driver, String locator, String attributeName, String... values) {
		locator = String.format(locator, (Object[]) values);
		element = driver.findElement(By.xpath(locator));
		return element.getAttribute(attributeName);
	}

	public boolean isControlDisplayed(AndroidDriver<AndroidElement> driver, String locator) {
		boolean status = true;
		try {
			element = driver.findElement(By.xpath(locator));
			if (element.isDisplayed()) {
				return status;
			}
		} catch (Exception ex) {
			Reporter.log(
					"================================================================= Element not displayed ================================");
			Reporter.log(ex.getMessage());
			System.err.println(
					"================================================================= Element not displayed ================================");
			System.err.println(ex.getMessage());
			status = false;
		}
		return status;
	}

	@SuppressWarnings("rawtypes")
	public void tapToElement(AndroidDriver<AndroidElement> driver, String locator) {
		element = driver.findElement(By.xpath(locator));
		touch = new TouchAction(driver);
		touch.tap(tapOptions().withElement(element(element))).perform();
	}

	@SuppressWarnings("rawtypes")
	public void tapToElement(AndroidDriver<AndroidElement> driver, String locator, String... values) {
		locator = String.format(locator, (Object[]) values);
		element = driver.findElement(By.xpath(locator));
		touch = new TouchAction(driver);
		touch.tap(tapOptions().withElement(element(element))).perform();
	}

	@SuppressWarnings("rawtypes")
	public void tapAndHoldToElement(AndroidDriver<AndroidElement> driver, String locator, int timeHoldOn) {
		element = driver.findElement(By.xpath(locator));
		touch = new TouchAction(driver);
		touch.longPress(longPressOptions().withElement(element(element)).withDuration(ofSeconds(timeHoldOn))).release()
				.perform();
	}

	@SuppressWarnings("rawtypes")
	public void moveElementToElement(AndroidDriver<AndroidElement> driver, String locator, String locator1,
			int timeHoldOn) {
		element = driver.findElement(By.xpath(locator));
		element1 = driver.findElement(By.xpath(locator1));
		touch = new TouchAction(driver);
		touch.longPress(longPressOptions().withElement(element(element)).withDuration(ofSeconds(timeHoldOn)))
				.moveTo(element(element1)).release().perform();
	}

	@SuppressWarnings("rawtypes")
	public void moveToElement(AndroidDriver<AndroidElement> driver, String string) {
		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""
						+ string + "\").instance(0))");
	}

	@SuppressWarnings("rawtypes")
	public void moveToElement(AndroidDriver<AndroidElement> driver, String locator, String... values) {
		locator = String.format(locator, (Object[]) values);
		element = driver.findElement(By.xpath(locator));
		touch = new TouchAction(driver);
		touch.moveTo(element(element)).perform();
	}

	public void clickToMenuButton(AndroidDriver<AndroidElement> driver) {
		waitForElementVisible(driver, DashboardPageUI.MENU_BUTTON);
		tapToElement(driver, DashboardPageUI.MENU_BUTTON);
//		clickToElement(driver, DashboardPageUI.FAB_BUTTON);
	}

	public AbstractPage clickToDynamicMenuLink(AndroidDriver<AndroidElement> driver, String string) {
		waitForElementVisible(driver, SwipeBarMenuUI.DYNAMIC_MENU_LINK, string);
		tapToElement(driver, SwipeBarMenuUI.DYNAMIC_MENU_LINK, string);
		switch (string) {
		case "Reports":
			return PageFactoryManage.getReportPage(driver);
		case "Activity":
			return PageFactoryManage.getActivityPage(driver);
		case "Settings":
			return PageFactoryManage.getSettingsPage(driver);
		case "Invoicing":
			return PageFactoryManage.getInvoicingPage(driver);
		case "My Accountant":
			return PageFactoryManage.getMyAccountantPage(driver);
		case "My Clients":
			return PageFactoryManage.getMyClientPage(driver);
		default:
			return PageFactoryManage.getReportPage(driver);
		}
//		clickToElement(driver, DashboardPageUI.FAB_BUTTON);
	}

}
