package commons;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountant.pageObjects.ActivityPageObject;
import myaccountant.pageObjects.AddCustomerPageObject;
import myaccountant.pageObjects.AddIncomeExpensePaymentPageObject;
import myaccountant.pageObjects.AddInvoicePageObject;
import myaccountant.pageObjects.AddItemPageObject;
import myaccountant.pageObjects.AuthenticationPageObject;
import myaccountant.pageObjects.DashboardPageObject;
import myaccountant.pageObjects.EditTransactionPageObject;
import myaccountant.pageObjects.InvoicePageObject;
import myaccountant.pageObjects.LoginPageObject;
import myaccountant.pageObjects.MyAccountantPageObject;
import myaccountant.pageObjects.MyClientPageObject;
import myaccountant.pageObjects.ReportPageObject;
import myaccountant.pageObjects.SettingsPageObject;
import myaccountant.pageObjects.SignUpAsAccountPageObject;
import myaccountant.pageObjects.SignUpPageObject;

public class PageFactoryManage {

	public static AuthenticationPageObject getAuthenticationPage(AndroidDriver<AndroidElement> driver) {
		return new AuthenticationPageObject(driver);
	}

	public static LoginPageObject getLoginPage(AndroidDriver<AndroidElement> driver) {
		return new LoginPageObject(driver);
	}

	public static DashboardPageObject getDashboardPage(AndroidDriver<AndroidElement> driver) {
		return new DashboardPageObject(driver);
	}
	
	public static AddIncomeExpensePaymentPageObject getIncomeExpensePaymentPage(AndroidDriver<AndroidElement> driver) {
		return new AddIncomeExpensePaymentPageObject(driver);
	}
	
	public static SignUpPageObject getSignUpPage(AndroidDriver<AndroidElement> driver) {
		return new SignUpPageObject(driver);
	}
	
	public static SignUpAsAccountPageObject getSignUpAsAccountantPage(AndroidDriver<AndroidElement> driver) {
		return new SignUpAsAccountPageObject(driver);
	}
	
	public static ReportPageObject getReportPage(AndroidDriver<AndroidElement> driver) {
		return new ReportPageObject(driver);
	}
	
	public static EditTransactionPageObject getEditTransactionPage(AndroidDriver<AndroidElement> driver) {
		return new EditTransactionPageObject(driver);
	}
	
	public static ActivityPageObject getActivityPage(AndroidDriver<AndroidElement> driver) {
		return new ActivityPageObject(driver);
	}
	
	public static SettingsPageObject getSettingsPage(AndroidDriver<AndroidElement> driver) {
		return new SettingsPageObject(driver);
	}
	
	public static AddItemPageObject getItemPage(AndroidDriver<AndroidElement> driver) {
		return new AddItemPageObject(driver);
	}
	
	public static AddCustomerPageObject getCustomerPage(AndroidDriver<AndroidElement> driver) {
		return new AddCustomerPageObject(driver);
	}
	
	public static AddInvoicePageObject getInvoicePage(AndroidDriver<AndroidElement> driver) {
		return new AddInvoicePageObject(driver);
	}
	
	public static InvoicePageObject getInvoicingPage(AndroidDriver<AndroidElement> driver) {
		return new InvoicePageObject(driver);
	}
	
	public static MyAccountantPageObject getMyAccountantPage(AndroidDriver<AndroidElement> driver) {
		return new MyAccountantPageObject(driver);
	}
	
	public static MyClientPageObject getMyClientPage(AndroidDriver<AndroidElement> driver) {
		return new MyClientPageObject(driver);
	}
}
