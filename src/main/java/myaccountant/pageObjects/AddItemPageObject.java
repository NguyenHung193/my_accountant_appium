package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.AddIncomeExpensePaymentPageUI;
import myaccountantUI.AddItemPageUI;
import myaccountantUI.AuthenticationPageUI;

public class AddItemPageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public AddItemPageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}
	
	public LoginPageObject clickToLoginButton () {
		waitForElementVisible(driver, AuthenticationPageUI.LOGIN_BUTTON);
		tapToElement(driver, AuthenticationPageUI.LOGIN_BUTTON);
		return PageFactoryManage.getLoginPage(driver);
	}
	
	public void sendkeysToName (String value) {
		waitForElementVisible(driver, AddItemPageUI.ENTER_NAME);
		sendkeyToElement(driver, AddItemPageUI.ENTER_NAME, value);
	}
	
	public void sendkeysToPrice (String value) {
		waitForElementVisible(driver, AddItemPageUI.ENTER_PRICE);
		sendkeyToElement(driver, AddItemPageUI.ENTER_PRICE, value);
	}
	
	public void selectCategory () {
		waitForElementVisible(driver, AddItemPageUI.SELECT_CATEGORY);
		tapToElement(driver, AddItemPageUI.SELECT_CATEGORY);
	}
	
	public void selectDynamicCategory (String value) {
		waitForElementVisible(driver, AddItemPageUI.SELECT_DYNAMIC_CATEGORY);
		tapToElement(driver, AddItemPageUI.SELECT_DYNAMIC_CATEGORY,value);
	}
	
	public void submitTransaction () {
		waitForElementVisible(driver, AddItemPageUI.SUBMIT_BUTTON);
		tapToElement(driver, AddItemPageUI.SUBMIT_BUTTON);
	}
	
	public InvoicePageObject selectOkButton () {
		waitForElementVisible(driver, AddItemPageUI.OK_BUTTON);
		tapToElement(driver, AddItemPageUI.OK_BUTTON);
		return PageFactoryManage.getInvoicingPage(driver);
	}
}
