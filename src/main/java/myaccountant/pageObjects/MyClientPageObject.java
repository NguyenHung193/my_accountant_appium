package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.AuthenticationPageUI;
import myaccountantUI.LoginPageUI;
import myaccountantUI.MyAccountantPageUI;
import myaccountantUI.MyClientPageUI;

public class MyClientPageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public MyClientPageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}
	
	public void clickToOkButton () {
		waitForElementVisible(driver, MyClientPageUI.OK_BUTTON);
		clickToElement(driver, MyClientPageUI.OK_BUTTON);
	}
	
	public void clickDynamicClientTab (String value) {
		waitForElementVisible(driver, MyClientPageUI.DYNAMIC_CLIENTS_TAB, value);
		clickToElement(driver, MyClientPageUI.DYNAMIC_CLIENTS_TAB, value);
	}
	
	public void clickFirstClientButton () {
		waitForElementVisible(driver, MyClientPageUI.FIRST_CLIENT);
		clickToElement(driver, MyClientPageUI.FIRST_CLIENT);
	}
	
	public void clickApproveButton () {
		waitForElementVisible(driver, MyClientPageUI.APPROVE_BUTTON);
		clickToElement(driver, MyClientPageUI.APPROVE_BUTTON);
	}
	
	public void clickBackButton () {
		waitForElementVisible(driver, MyClientPageUI.BACK_BUTTON);
		clickToElement(driver, MyClientPageUI.BACK_BUTTON);
	}
	
	public void clickToCloseListAccountantButton () {
		waitForElementVisible(driver, MyAccountantPageUI.CLOSE_LIST_ACCOUNT_BUTTON);
		clickToElement(driver, MyAccountantPageUI.CLOSE_LIST_ACCOUNT_BUTTON);
	}
	
	public AuthenticationPageObject clickToCloseIcon () {
		waitForElementVisible(driver, LoginPageUI.CLOSE_BUTTON);
		clickToElement(driver, LoginPageUI.CLOSE_BUTTON);
		return PageFactoryManage.getAuthenticationPage(driver);
	}
}
