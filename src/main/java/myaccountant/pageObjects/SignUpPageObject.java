package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.AuthenticationPageUI;
import myaccountantUI.LoginPageUI;
import myaccountantUI.SignUpPageUI;

public class SignUpPageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public SignUpPageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}
	
	public void sendkeyToDynamicSignUpInput (String valueToSendkey , String string) {
		waitForElementVisible(driver, SignUpPageUI.DYNAMIC_SIGNUP_INPUT, string);
		clearElement(driver, SignUpPageUI.DYNAMIC_SIGNUP_INPUT, string);
		sendkeyToElement(driver, SignUpPageUI.DYNAMIC_SIGNUP_INPUT, valueToSendkey , string);
	}
	
	public void clickToNextButton () {
		waitForElementVisible(driver, SignUpPageUI.NEXT_BUTTON);
		clickToElement(driver, SignUpPageUI.NEXT_BUTTON);
	}
	
	public void waitForMailWasSent () {
		sleepInSecond(30);
	}
	
	public void clickToVerifynButton () {
		waitForElementVisible(driver, SignUpPageUI.VERIFY_BUTTON);
		clickToElement(driver, SignUpPageUI.VERIFY_BUTTON);
	}
	
	public DashboardPageObject clickToSkipItButton () {
		waitForElementVisible(driver, SignUpPageUI.SKIP_IT_BUTTON);
		clickToElement(driver, SignUpPageUI.SKIP_IT_BUTTON);
		return PageFactoryManage.getDashboardPage(driver);
	}
}
