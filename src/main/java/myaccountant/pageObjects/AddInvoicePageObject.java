package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.AddIncomeExpensePaymentPageUI;
import myaccountantUI.AddInvoicePageUI;
import myaccountantUI.AuthenticationPageUI;

public class AddInvoicePageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public AddInvoicePageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}
	
	public void clickToAddCustomerButton () {
		waitForElementVisible(driver, AddInvoicePageUI.ADD_CUSTOMER);
		tapToElement(driver, AddInvoicePageUI.ADD_CUSTOMER);
	}
	
	public void selectFirstItemCustomer () {
		waitForElementVisible(driver, AddInvoicePageUI.SELECT_FIRST_ITEM_CUSTOMER);
		tapToElement(driver, AddInvoicePageUI.SELECT_FIRST_ITEM_CUSTOMER);
	}
	
	public void clickToAddItemButton () {
		waitForElementVisible(driver, AddInvoicePageUI.ADD_ITEM);
		tapToElement(driver, AddInvoicePageUI.ADD_ITEM);
	}
	
	public void selectSubmitButton () {
		waitForElementVisible(driver, AddInvoicePageUI.SUBMIT_BUTTON);
		tapToElement(driver, AddInvoicePageUI.SUBMIT_BUTTON);
	}
	
	public void selectDynamicButton (String value) {
		waitForElementVisible(driver, AddInvoicePageUI.DYNAMIC_BUTTON, value);
		tapToElement(driver, AddInvoicePageUI.DYNAMIC_BUTTON, value);
	}
	
	public void selectStatusCategory () {
		waitForElementVisible(driver, AddInvoicePageUI.STATUS_CATEGORY);
		tapToElement(driver, AddInvoicePageUI.STATUS_CATEGORY);
	}
	
	public void selectDynamicStatus (String value) {
		waitForElementVisible(driver, AddInvoicePageUI.SELECT_DYNAMIC_STATUS, value);
		tapToElement(driver, AddInvoicePageUI.SELECT_DYNAMIC_STATUS, value);
	}
	
	public void clickOkButton () {
		waitForElementVisible(driver, AddInvoicePageUI.OK_BUTTON);
		tapToElement(driver, AddInvoicePageUI.OK_BUTTON);
	}
	
}
