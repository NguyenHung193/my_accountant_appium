package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.AuthenticationPageUI;
import myaccountantUI.LoginPageUI;
import myaccountantUI.SignUpAsAccountantPageUI;
import myaccountantUI.SignUpPageUI;

public class SignUpAsAccountPageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public SignUpAsAccountPageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}
	
	public void sendkeyToDynamicSignUpInput (String valueToSendkey , String string) {
		waitForElementVisible(driver, SignUpAsAccountantPageUI.DYNAMIC_SIGNUP_INPUT, string);
		clearElement(driver, SignUpAsAccountantPageUI.DYNAMIC_SIGNUP_INPUT, string);
		sendkeyToElement(driver, SignUpAsAccountantPageUI.DYNAMIC_SIGNUP_INPUT, valueToSendkey , string);
	}
	
	public void clickToNextButton () {
		waitForElementVisible(driver, SignUpAsAccountantPageUI.NEXT_BUTTON);
		clickToElement(driver, SignUpAsAccountantPageUI.NEXT_BUTTON);
	}
	
	public void waitForMailWasSent () {
		sleepInSecond(30);
	}
	
	public void clickToVerifynButton () {
		waitForElementVisible(driver, SignUpAsAccountantPageUI.VERIFY_BUTTON);
		clickToElement(driver, SignUpAsAccountantPageUI.VERIFY_BUTTON);
	}
	
	public void clickToSignUpButton () {
		waitForElementVisible(driver, SignUpAsAccountantPageUI.SIGNUP_BUTTON);
		clickToElement(driver, SignUpAsAccountantPageUI.SIGNUP_BUTTON);
	}
	
	public DashboardPageObject clickToSkipItButton () {
		waitForElementVisible(driver, SignUpAsAccountantPageUI.SKIP_IT_BUTTON);
		clickToElement(driver, SignUpAsAccountantPageUI.SKIP_IT_BUTTON);
		return PageFactoryManage.getDashboardPage(driver);
	}
}
