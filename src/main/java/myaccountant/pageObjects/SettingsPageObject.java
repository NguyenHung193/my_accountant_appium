package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.DashboardPageUI;
import myaccountantUI.SettingsPageUI;

public class SettingsPageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public SettingsPageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}

	public void sendkeyToDynamicMenuSettingsLink(String locator) {
		waitForElementVisible(driver, SettingsPageUI.DYNAMIC_MENU_LINK, locator);
		clickToElement(driver, SettingsPageUI.DYNAMIC_MENU_LINK, locator);
	}

	public void clickToEnableInvoicingButton() {
		waitForElementVisible(driver, SettingsPageUI.ENABLE_INVOICING);
		clickToElement(driver, SettingsPageUI.ENABLE_INVOICING);
	}

	public void backToSettings() {
		waitForElementVisible(driver, SettingsPageUI.BACK_BUTTON);
		tapToElement(driver, SettingsPageUI.BACK_BUTTON);
//		clickToElement(driver, DashboardPageUI.FAB_BUTTON);
	}
	
	public void backToConfirmLogout() {
		waitForElementVisible(driver, SettingsPageUI.LOGOUT_BUTTON);
		tapToElement(driver, SettingsPageUI.LOGOUT_BUTTON);
//		clickToElement(driver, DashboardPageUI.FAB_BUTTON);
	}
	
}
