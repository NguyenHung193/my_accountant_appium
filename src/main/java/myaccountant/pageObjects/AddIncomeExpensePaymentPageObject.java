package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.AddIncomeExpensePaymentPageUI;
import myaccountantUI.AuthenticationPageUI;

public class AddIncomeExpensePaymentPageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public AddIncomeExpensePaymentPageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}
	
	public void clickToDynamicButton (String locator) {
		waitForElementVisible(driver, locator);
		clickToElement(driver, locator);
	}
	
	public LoginPageObject clickToLoginButton () {
		waitForElementVisible(driver, AuthenticationPageUI.LOGIN_BUTTON);
		tapToElement(driver, AuthenticationPageUI.LOGIN_BUTTON);
		return PageFactoryManage.getLoginPage(driver);
	}
	
	public void sendkeysToAmount (String value) {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.AMOUNT);
		sendkeyToElement(driver, AddIncomeExpensePaymentPageUI.AMOUNT, value);
	}
	
	public void selectCategoryValue (String value) {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.DYNAMIC_CATEGORY_VALUE,value);
		tapToElement(driver, AddIncomeExpensePaymentPageUI.DYNAMIC_CATEGORY_VALUE,value);
	}
	
	public void selectCategory () {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.CATEGORY_SELECT);
		tapToElement(driver, AddIncomeExpensePaymentPageUI.CATEGORY_SELECT);
	}
	
	public void submitTransaction () {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.SUBMIT_TRANSACTION);
		tapToElement(driver, AddIncomeExpensePaymentPageUI.SUBMIT_TRANSACTION);
	}
	
	public void selectCash () {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.CASH_SELECT);
		tapToElement(driver, AddIncomeExpensePaymentPageUI.CASH_SELECT);
	}
	
	public void selectMoneyIn () {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.MONEY_IN_SELECT);
		tapToElement(driver, AddIncomeExpensePaymentPageUI.MONEY_IN_SELECT);
	}
	
	public void selectOkButton () {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.OK_BUTTON);
		tapToElement(driver, AddIncomeExpensePaymentPageUI.OK_BUTTON);
	}
	
	public void selectMoneyOut () {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.MONEY_OUT_SELECT);
		tapToElement(driver, AddIncomeExpensePaymentPageUI.MONEY_OUT_SELECT);
	}
	
	public void selectCredit () {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.CREDIT_SELECT);
		tapToElement(driver, AddIncomeExpensePaymentPageUI.CREDIT_SELECT);
	}
}
