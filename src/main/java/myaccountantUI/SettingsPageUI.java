package myaccountantUI;

public class SettingsPageUI {

	public static final String DYNAMIC_MENU_LINK = "//android.widget.TextView[@text='%s']";
	public static final String ENABLE_INVOICING = "//b.i.a.b//android.view.ViewGroup//android.view.ViewGroup[@index='1']//android.widget.ScrollView//android.view.ViewGroup[@index='2']//android.view.ViewGroup[@index='3']//android.widget.TextView";
	public static final String BACK_BUTTON = "//b.i.a.b//android.widget.FrameLayout//android.widget.ImageButton";
	public static final String LOGOUT_BUTTON = "//android.widget.FrameLayout[@resource-id='android:id/content']//android.widget.LinearLayout//android.widget.ScrollView[@resource-id='android:id/buttonPanel']//android.widget.Button[@index='1']";
	public static final String ADD_ITEM_BUTTON = "//android.widget.TextView[@text='Add Item']";
	public static final String ADD_CUSTOMER_BUTTON = "//android.widget.TextView[@text='Add Customer']";
	public static final String ADD_INVOICE_BUTTON = "//android.widget.TextView[@text='Add Invoice']";
}
