package myaccountantUI;

public class LoginPageUI {

	public static final String EMAIL_PASSWORD_INPUT = "//android.widget.EditText[@text='%s']";
//	public static final String EMAIL_PASSWORD_INPUT = "//android.widget.EditText[@content-desc='%s']";
	public static final String PASSWORD_INPUT = "//android.widget.EditText[@text='PASSWORD']";
//	public static final String LOGIN_BUTTON = "//android.view.ViewGroup[@content-desc='LoginBtn, ']";
	public static final String LOGIN_BUTTON = "//android.widget.TextView[@text='LOGIN']";
	
	public static final String EMAIL_EMPTY_MESSAGE = "//android.widget.TextView[@text='Email can not be empty.']";
	public static final String EMAIL_NOT_CORRECT_MESSAGE = "//android.widget.TextView[@text='Email not correct.']";
	public static final String EMAIL_NOT_FOUND_MESSAGE = "//android.widget.TextView[@text='Email not found.']";
	public static final String PASSWORD_EMPTY_MESSAGE = "//android.widget.TextView[@text='Password can not be empty.']";
	public static final String PASSWORD_IN_CORRECT_MESSAGE = "//android.widget.TextView[@text='Invalid credentials.']";
	public static final String PASSWORD_LESS_THAN_6_DIGIT_MESSAGE = "//android.widget.TextView[@text='Password must have at least 6 characters.']";
	public static final String OK_BUTTON = "//android.widget.Button[@text='OK']";
	public static final String ICON = "//android.view.ViewGroup//android.widget.ImageView";
	public static final String CLOSE_BUTTON = "//android.view.ViewGroup//android.widget.TextView[@text='k']";
	
}
