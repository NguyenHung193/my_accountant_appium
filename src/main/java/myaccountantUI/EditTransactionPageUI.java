package myaccountantUI;

public class EditTransactionPageUI {

	public static final String DELETE_BUTTON = "//androidx.appcompat.widget.LinearLayoutCompat//android.widget.TextView[@index='0']";
	public static final String SUBMIT_BUTTON = "//androidx.appcompat.widget.LinearLayoutCompat//android.widget.TextView[@index='1']";
	public static final String CONFIRM_DELETE_BUTTON = "//android.widget.Button[@text='DELETE']";
}
