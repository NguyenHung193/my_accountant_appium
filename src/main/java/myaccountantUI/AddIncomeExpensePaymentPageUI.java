package myaccountantUI;

public class AddIncomeExpensePaymentPageUI {

	public static final String CASH_SELECT = "//android.widget.TextView[@text='Cash']";
	public static final String MONEY_IN_SELECT = "//android.widget.TextView[@text='Money In']";
	public static final String MONEY_OUT_SELECT = "//android.widget.TextView[@text='Money Out']";
	public static final String CREDIT_SELECT = "//android.widget.TextView[@text='Credit']";
	public static final String CATEGORY_SELECT = "//android.widget.TextView[@text='y']";
	public static final String DYNAMIC_CATEGORY_VALUE = "//android.widget.TextView[@text='%s']";
	public static final String AMOUNT = "//android.widget.ScrollView//android.view.ViewGroup//android.widget.EditText";
	public static final String SUBMIT_TRANSACTION = "//androidx.appcompat.widget.LinearLayoutCompat//android.widget.TextView";
	public static final String OK_BUTTON = "//android.widget.Button[@resource-id='android:id/button1']";
}
