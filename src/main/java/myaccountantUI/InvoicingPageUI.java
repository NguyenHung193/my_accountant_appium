package myaccountantUI;

public class InvoicingPageUI {

	public static final String FAB_BUTTON = "//android.widget.TextView[@text='+']";
	public static final String MENU_BUTTON = "//android.widget.FrameLayout//android.widget.ImageButton";
	public static final String ADD_INCOME_BUTTON = "//android.widget.TextView[@text='Add Income']";
	public static final String ADD_EXPENSE_BUTTON = "//android.widget.TextView[@text='Add Expense']";
	public static final String ADD_PAYMENT_BUTTON = "//android.widget.TextView[@text='Add Payment']";
	public static final String ADD_ITEM_BUTTON = "//android.widget.TextView[@text='Add Item']";
	public static final String ADD_CUSTOMER_BUTTON = "//android.widget.TextView[@text='Add Customer']";
	public static final String ADD_INVOICE_BUTTON = "//android.widget.TextView[@text='Add InvoiceDashboardPageUI.java']";
}
