package myaccountantUI;

public class SignUpAsAccountantPageUI {

	public static final String DYNAMIC_SIGNUP_INPUT = "//android.widget.EditText[@text='%s']";
	public static final String NEXT_BUTTON = "//android.widget.TextView[@text='NEXT']";
	public static final String VERIFY_BUTTON = "//android.widget.TextView[@text='Verify']";
	public static final String SIGNUP_BUTTON = "//android.widget.TextView[@text='SIGN UP']";
	public static final String SKIP_IT_BUTTON = "//android.widget.TextView[@text='Skip it']";
}
