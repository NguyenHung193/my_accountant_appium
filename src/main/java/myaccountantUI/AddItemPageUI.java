package myaccountantUI;

public class AddItemPageUI {

	public static final String ENTER_NAME = "//b.i.a.b//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup//android.widget.EditText[@index='1']";
	public static final String ENTER_PRICE = "//android.widget.EditText[@text='0']";
	public static final String SELECT_CATEGORY = "//android.widget.TextView[@text='CATEGORY']";
	public static final String SELECT_DYNAMIC_CATEGORY = "//android.widget.TextView[@text='%s']";
	public static final String SUBMIT_BUTTON = "//androidx.appcompat.widget.LinearLayoutCompat//android.widget.TextView";
	public static final String OK_BUTTON = "//android.widget.Button[@resource-id='android:id/button1']";
}
