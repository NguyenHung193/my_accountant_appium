package myaccountantUI;

public class MyClientPageUI {

	public static final String OK_BUTTON = "//android.widget.Button[@resource-id='android:id/button1']";
	public static final String DYNAMIC_CLIENTS_TAB = "//b.i.a.b//android.view.ViewGroup//android.widget.FrameLayout//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup[@index='2']//android.widget.TextView[@text='%s']";
	public static final String ADD_ACCOUNTANT_BUTTON = "//androidx.appcompat.widget.LinearLayoutCompat//android.widget.TextView";
	public static final String FIRST_CLIENT = "//b.r.a.f//android.widget.ScrollView//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup";
	public static final String APPROVE_BUTTON = "//android.widget.TextView[@text='Approve']";
	public static final String BACK_BUTTON = "//android.view.ViewGroup//android.widget.TextView[@text='g']";
	public static final String CLOSE_LIST_ACCOUNT_BUTTON = "//android.widget.FrameLayout[@resource-id='android:id/content']//android.view.ViewGroup[@index='2']//android.view.ViewGroup//android.widget.FrameLayout//android.view.ViewGroup//android.widget.ImageButton";
}
