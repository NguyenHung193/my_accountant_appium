package cucumberOptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.UnreachableBrowserException;

import commons.Constants;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Hooks {
	// Run for many thread
//	private static WebDriver driver;
	protected static AndroidDriver<AndroidElement> driver;
	private static final Logger log = Logger.getLogger(Hooks.class.getName());
	public static AppiumDriverLocalService service;
	private final static String workingDir = System.getProperty("user.dir");

	
	public static AppiumDriverLocalService startServer() {

		//
		boolean flag = checkIfServerIsRunnning(4723);
		if (!flag) {

			service = AppiumDriverLocalService.buildDefaultService();
			service.start();
		}
		return service;

	}

	public static boolean checkIfServerIsRunnning(int port) {

		boolean isServerRunning = false;
		ServerSocket serverSocket;
		try {
			serverSocket = new ServerSocket(port);

			serverSocket.close();
		} catch (IOException e) {
			// If control comes here, then it means that the port is in use
			isServerRunning = true;
		} finally {
			serverSocket = null;
		}
		return isServerRunning;
	}

	public static void startEmulator() {

		try {
			Runtime.getRuntime().exec(workingDir + "\\src\\main\\java\\startEmulator.bat");
			Thread.sleep(6000);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void killAllNodes() {
		try {
			Runtime.getRuntime().exec("taskkill /F /IM node.exe");
			Thread.sleep(3000);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Before
	public static AndroidDriver<AndroidElement> openAndroidDevice(String appName) {
		killAllNodes();
		service = startServer();
		try {
			FileInputStream fis = new FileInputStream(workingDir + "\\src\\main\\java\\global.properties");
			Properties prop = new Properties();
			prop.load(fis);

			File f = new File(workingDir + "\\src\\main\\resources\\appInstall");
			File fs = new File(f, (String) prop.get(appName));
			DesiredCapabilities cap = new DesiredCapabilities();
			String device = (String) prop.get("GooglePixel3");

			if (device.contains("Bi")) {
				startEmulator();
				System.out.println("+++++++++++++++++++++++++++++++++++++++++++++Device");
			}

			cap.setCapability(MobileCapabilityType.DEVICE_NAME, device);
			cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
			cap.setCapability("noResetValue","false");
			cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 10000);

			cap.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath());
			driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			return driver;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return driver;
		}
	}
	
	@After
	public void quitBrowser() {
		service.stop();
//		((AppiumDriver)driver).closeApp();
	}
	
//	@Before
//	public synchronized static WebDriver openAndQuitBrowser() {
//		// Run by Maven command line
//		String browser = System.getProperty("BROWSER");
//
//		// Check driver đã được khởi tạo hay chưa?
//		if (driver == null) {
//			try {
//				// Kiem tra BROWSER = null -> gan = chrome/ firefox (browser default for project)
//				if (browser == null) {
//					browser = System.getenv("BROWSER");
//					if (browser == null) {
//						browser = "firefox";
//					}
//				}
//
//				switch (browser) {
//				case "chrome":
//					WebDriverManager.chromedriver().version("2.46").setup();
//					driver = new ChromeDriver();
//					break;
//				case "hchrome":
//					WebDriverManager.chromedriver().version("2.46").setup();
//					ChromeOptions options = new ChromeOptions();
//					options.addArguments("headless");
//					options.addArguments("window-size=1366x768");
//					driver = new ChromeDriver(options);
//					break;
//				case "firefox":
//					// Download gecko driver
//					WebDriverManager.firefoxdriver().setup();
//					driver = new FirefoxDriver();
//					break;
//				case "hfirefox":
//					WebDriverManager.firefoxdriver().setup();
//					driver = new FirefoxDriver();
//					break;
//				case "ie":
//					WebDriverManager.iedriver().arch32().setup();
//					driver = new InternetExplorerDriver();
//					break;
//				default:
//					WebDriverManager.chromedriver().version("2.46").setup();
//					driver = new ChromeDriver();
//					break;
//				}
//			// Browser crash
//			} catch (UnreachableBrowserException e) {
//				driver = new ChromeDriver();
//			// driver crash
//			} catch (WebDriverException e) {
//				driver = new ChromeDriver();
//			}
//			// Code này luôn luôn được chạy dù pass hay fail
//			finally {
//				Runtime.getRuntime().addShutdownHook(new Thread(new BrowserCleanup()));
//			}
//
//			System.out.println("Browser name = " + browser);
//			
//			driver.get(Constants.DYNAMIC_MERCHANT_ADMIN_URL);
////			driver.manage().window().maximize();
//			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
//			log.info("------------- Started the browser -------------");
//		}
//		return driver;
//	}

//	@After
//	public static void quitBrowser() {
//		if (driver != null) {
//			driver.quit();
//		}
//	}

//	public static void close() {
//		try {
//			if (driver != null) {
//				openAndQuitBrowser().quit();
//				log.info("------------- Closed the browser -------------");
//			}
//		} catch (UnreachableBrowserException e) {
//			System.out.println("Can not close the browser");
//		}
//	}
//
//	private static class BrowserCleanup implements Runnable {
//		public void run() {
//			close();
//		}
//	}

}