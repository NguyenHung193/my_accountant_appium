package stepDefinitions;

import commons.AbstractTest;
import commons.PageFactoryManage;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberOptions.Hooks;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountant.pageObjects.AuthenticationPageObject;
import myaccountant.pageObjects.DashboardPageObject;
import myaccountant.pageObjects.LoginPageObject;


 
public class LoginSteps extends AbstractTest{
	private AndroidDriver<AndroidElement> driver;
	private LoginPageObject loginPage;
	private AuthenticationPageObject authenPage;
	private DashboardPageObject dashboardPage;
	
	String correctEmail, incorrectEmail, incorrectFormatEmail, correctPassword, incorrectPassword;
	
	public LoginSteps() {
		
		correctEmail = "hung.nguyen@enouvo.com";
		incorrectEmail = "hungnguyen@enouvo.com";
		correctPassword = "123456";
		incorrectPassword = "12345678";
		incorrectFormatEmail = "hung.nguyen";

		
		driver = Hooks.openAndroidDevice("MyAccountantStaging");
		
		authenPage = PageFactoryManage.getAuthenticationPage(driver);
		
	}
	
	@Given("^Open device and app$")
	public void openDeviceAndApp() {

	}
	
	@When("^I click on signIn button$")
	public void iClickOnSignInButton() {
		
		loginPage = authenPage.clickToLoginButton();
	}

	@When("^I enter email$")
	public void iEnterEmail() {
		
		loginPage.sendkeyToDynamicInput("hung.nguyen@enouvo.com", "EMAIL");
	}

	@When("^I enter password$")
	public void iEnterPassword() {
		
		loginPage.sendkeyToDynamicInput("123456", "PASSWORD");
	}

	@When("^I click next button$")
	public void iClickNextButton() {
		
		dashboardPage = loginPage.clickToLoginButton();
	      
	}

	@Then("^I verify account has been logged in successful$")
	public void iVerifyAccountHasBeenLoggedInSuccessful() {
	    verifyTrue(dashboardPage.isMenuDisplayed());
	}

	@Given("^I enter empty email$")
	public void iEnterEmptyEmail() {

		loginPage.sendkeyToDynamicInput("", "EMAIL");

	}

	@Given("^I enter empty password$")
	public void iEnterEmptyPassword() {

		loginPage.sendkeyToDynamicInput("", "PASSWORD");

	}

	@Given("^I click next button but not success$")
	public void iClickNextButtonButNotSuccess() {

		loginPage.clickToLoginButtonNotValid();

	}

	@Then("^I verify email is empty message displayed$")
	public void iVerifyEmailIsEmptyMessageDisplayed() {

		verifyTrue(loginPage.isEmailEmptyMessageDisplayed());

	}

	@Then("^I enter correct password$")
	public void iEnterCorrectPassword() {

		loginPage.sendkeyToDynamicInput(correctPassword, "PASSWORD");

	}

	@Then("^I enter incorrect email$")
	public void iEnterIncorrectEmail() {

		loginPage.sendkeyToDynamicInput(incorrectEmail, "EMAIL");

	}

	@Then("^I verify email is not found message displayed$")
	public void iVerifyEmailIsNotFoundMessageDisplayed() {

		verifyTrue(loginPage.isEmailNotFoundMessageDisplayed());

	}

	@Then("^I enter correct email$")
	public void iEnterCorrectEmail() {

		loginPage.sendkeyToDynamicInput(correctEmail, "EMAIL");

	}

	@Then("^I enter incorrect password$")
	public void iEnterIncorrectPassword() {

		loginPage.sendkeyToDynamicInput(incorrectPassword, "PASSWORD");

	}

	@Then("^I verify password is incorrect message displayed$")
	public void iVerifyPasswordIsIncorrectMessageDisplayed() {

		verifyTrue(loginPage.isPasswordInCorrectMessageDisplayed());

	}

	@Then("^I enter password less than six digit$")
	public void iEnterPasswordLessThanSixDigit() {

		loginPage.sendkeyToDynamicInput("123", "PASSWORD");

	}

	@Then("^I verify password is less than six digit message displayed$")
	public void iVerifyPasswordIsLessThanSixDigitMessageDisplayed() {

		verifyTrue(loginPage.isPasswordLessThan6DigitMessageDisplayed());

	}
	
	@Given("^I enter incorrect format email$")
	public void iEnterIncorrectFormatEmail()  {
		
		loginPage.sendkeyToDynamicInput(incorrectFormatEmail, "EMAIL");
		
	}

	@Then("^I verify email is not correct message displayed$")
	public void iVerifyEmailIsNotCorrectMessageDisplayed() {
		
		verifyTrue(loginPage.isEmailNotCorrectMessageDisplayed());
		
	}
	
	@Then("^I verify password is empty message displayed$")
	public void iVerifyPasswordIsEmptyMessageDisplayed() {
		
		verifyTrue(loginPage.isPasswordEmptyMessageDisplayed());
		
	}

	
//	@After
//	@Given("^I close service$")
//	public void iCloseService() {
//		
//		service.stop();
//		
//	}

}
