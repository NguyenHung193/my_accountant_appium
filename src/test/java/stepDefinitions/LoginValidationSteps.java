package stepDefinitions;

import commons.AbstractTest;
import commons.PageFactoryManage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberOptions.Hooks;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountant.pageObjects.AuthenticationPageObject;
import myaccountant.pageObjects.DashboardPageObject;
import myaccountant.pageObjects.LoginPageObject;

public class LoginValidationSteps extends AbstractTest {
	private AndroidDriver<AndroidElement> driver;
	private LoginPageObject loginPage;
	private AuthenticationPageObject authenPage;
	private DashboardPageObject dashboardPage;

	String correctEmail, incorrectEmail, incorrectFormatEmail, correctPassword, incorrectPassword;

	public LoginValidationSteps() {

		correctEmail = "hung.nguyen@enouvo.com";
		incorrectEmail = "hungnguyen@enouvo.com";
		correctPassword = "123456";
		incorrectPassword = "12345678";
		incorrectFormatEmail = "hung.nguyen";

	}
	


}
