package stepDefinitions;

import commons.AbstractTest;
import commons.PageFactoryManage;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberOptions.Hooks;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountant.pageObjects.AuthenticationPageObject;
import myaccountant.pageObjects.DashboardPageObject;
import myaccountant.pageObjects.SignUpPageObject;

public class SignUpSteps extends AbstractTest {
	private AndroidDriver<AndroidElement> driver;
	private AuthenticationPageObject authenticationPage;
	private DashboardPageObject dashboardPage;
	private SignUpPageObject signUpPage;

	String email = "hung.nguyen.enouvotest+" + "signup" + randomNumber() + "@gmail.com";
	String firstName, lastName, phoneNumber, password, confirmPassword, verifyCode;

	public SignUpSteps() {

		driver = Hooks.openAndroidDevice("MyAccountantStaging");

		authenticationPage = PageFactoryManage.getAuthenticationPage(driver);

		firstName = "Hung";
		lastName = "Nguyen";
		phoneNumber = "0905495922";
		password = "Hung0905495922";
		confirmPassword = "Hung0905495922";

	}

	@When("^I click on signUp button$")
	public void iClickOnSignUpButton() {

		signUpPage = authenticationPage.clickToSignUpButton();

	}

	@When("^I enter new firstname$")
	public void iEnterNewFirstname() {

		signUpPage.sendkeyToDynamicSignUpInput(firstName, "First name");

	}

	@When("^I enter new lastname$")
	public void iEnterNewLastname() {

		signUpPage.sendkeyToDynamicSignUpInput(lastName, "Last name");

	}

	@When("^I enter new email$")
	public void iEnterNewEmail() {

		signUpPage.sendkeyToDynamicSignUpInput(email, "Email");

	}

	@When("^I enter new phonenumber$")
	public void iEnterNewPhonenumber() {

		signUpPage.sendkeyToDynamicSignUpInput(phoneNumber, "Phone number");
	}

	@When("^I enter new password$")
	public void iEnterNewPassword() {

		signUpPage.sendkeyToDynamicSignUpInput(password, "Password");
	}

	@When("^I enter new confirm password$")
	public void iEnterNewConfirmPassword() {

		signUpPage.sendkeyToDynamicSignUpInput(confirmPassword, "Confirm Password");
	}

	@When("^I click next signup button$")
	public void iClickNextSignupButton() {

		signUpPage.clickToNextButton();

	}

	@When("^I am waiting for email was sent$")
	public void iAmWaitingForEmailWasSent() {

		signUpPage.waitForMailWasSent();

	}

	@When("^I get verify code on email and fill it$")
	public void iGetVerifyCodeOnEmailAndFillIt() {

		String str = "<span style=\"font-size: 22pt\">";
		String a = verifyMail("hung.nguyen.enouvotest@gmail.com", "Hung0905495922", "MyAccountant Email Verification");
		String b = a.substring(a.indexOf(str) + str.length(),
				a.indexOf("<span style=\"font-size: 22pt\">") + 6 + str.length());
		System.out.println("VerifyCode: " + b);
		verifyCode = b;

		signUpPage.sendkeyToDynamicSignUpInput(verifyCode, "Verify code");

		signUpPage.clickToVerifynButton();

		dashboardPage = signUpPage.clickToSkipItButton();

	}

	@Then("^I verify new account was created successful$")
	public void iVerifyNewAccountWasCreatedSuccessful() {

		verifyTrue(dashboardPage.isMenuDisplayed());
	}

}
