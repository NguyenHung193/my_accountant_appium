@account
Feature: Login validation

  Scenario: Send empty data
    Given Open device and app
    When I click on signIn button
    And I enter empty email
    And I enter empty password
    And I click next button but not success
    Then I verify email is empty message displayed

  #Scenario: Send empty email
    #Given Open device and app
    #When I click on signIn button
    #And I enter empty email
    #And I enter correct password
    #And I click next button but not success
    #Then I verify email is empty message displayed
#
  #Scenario: Send incorrect email format
    #Given Open device and app
    #When I click on signIn button
    #And I enter incorrect format email
    #And I enter correct password
    #And I click next button but not success
    #Then I verify email is not correct message displayed
#
  #Scenario: Send inccorect email
    #Given Open device and app
    #When I click on signIn button
    #And I enter incorrect email
    #And I enter correct password
    #And I click next button but not success
    #Then I verify email is not found message displayed
#
  #Scenario: Send inccorect password
    #Given Open device and app
    #When I click on signIn button
    #And I enter correct email
    #And I enter incorrect password
    #And I click next button but not success
    #Then I verify password is incorrect message displayed
#
  #Scenario: Send password less than six digit
    #Given Open device and app
    #When I click on signIn button
    #And I enter correct email
    #And I enter password less than six digit
    #And I click next button but not success
    #Then I verify password is less than six digit message displayed
#
  #Scenario: Send empty password
    #Given Open device and app
    #When I click on signIn button
    #And I enter correct email
    #And I enter empty password
    #And I click next button but not success
    #Then I verify password is empty message displayed
