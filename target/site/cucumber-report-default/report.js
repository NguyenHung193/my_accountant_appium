$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("LoginValidation.feature");
formatter.feature({
  "line": 2,
  "name": "Login validation",
  "description": "",
  "id": "login-validation",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@account"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Send empty data",
  "description": "",
  "id": "login-validation;send-empty-data",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "Open device and app",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I click on signIn button",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I enter empty email",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I enter empty password",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click next button but not success",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I verify email is empty message displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginSteps.openDeviceAndApp()"
});
formatter.result({
  "duration": 30275663600,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.iClickOnSignInButton()"
});
formatter.result({
  "duration": 2202826800,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.iEnterEmptyEmail()"
});
formatter.result({
  "duration": 2606185000,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.iEnterEmptyPassword()"
});
formatter.result({
  "duration": 2834437500,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.iClickNextButtonButNotSuccess()"
});
formatter.result({
  "duration": 1432194600,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.iVerifyEmailIsEmptyMessageDisplayed()"
});
formatter.result({
  "duration": 976635100,
  "status": "passed"
});
});