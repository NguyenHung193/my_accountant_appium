#@account
Feature: Signup function

  Scenario: Signup with new user
    Given Open device and app
    When I click on signUp button
    And I enter new firstname
    And I enter new lastname 
    And I enter new email 
    And I enter new phonenumber  
    And I enter new password 
    And I enter new confirm password 
    And I click next signup button
    And I am waiting for email was sent
		And I get verify code on email and fill it
		Then I verify new account was created successful
    
